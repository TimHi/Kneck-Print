# Kneck Print

Minimal screenshot tool for Linux, select rectangle and get the url copied into your    
clipboard.   


# Installation:   

>git clone https://github.com/TimHi/Kneck-Print.git  
>cd Kneck-Print  
>make KneckPrint  

Needs xsel installed to copy the link to your clipboard.

# Usage:  

>KneckPrint | xsel --clipboard   

Or if you want to bind it to a key use:   
>bash -c 'KneckPrint | xsel --clipboard'   

This program is using my Client-ID from the imgur API, if you are going to use  
the tool consider switching it to yours to avoid limitations.   

# Note:  

There are certain situations where the program wont work. Two examples are Dota2 and CS:GO in a game,   
my assumption is this is caused by the games already grabbing the XPointer.    
If you have any idea how to fix this please let me know, i spent way to many time trying to figure it out.
