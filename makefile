#KneckPrint
# What to call the final executable
TARGET = KneckPrint

# Which object files that the executable consists of
OBJS= KneckPrint.o

# What compiler to use
CC = g++

# Compiler flags, -g for debug, -c to make an object file
CFLAGS = -c -g

# This should point to a directory that holds libcurl, if it isn't
# in the system's standard lib dir
# We also set a -L to include the directory where we have the openssl
# libraries
LDFLAGS = -L/home/dast/lib -L/usr/local/ssl/lib

# We need -lcurl for the curl stuff
LIBS = -lcurl

# Link the target with all objects and libraries
$(TARGET) : $(OBJS)
	$(CC)  -o $(TARGET) $(OBJS) $(LDFLAGS) $(LIBS)

# Compile the source files into object files
KneckPrint.o : KneckPrint.cpp
	$(CC) $(CFLAGS) $<