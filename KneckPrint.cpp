#include <curl/curl.h>

/*  Upload existing image to imgur
    Change Client-ID if possible to avoid upload restrictions
    Consider rewriting this in a more c++'ish fashion.
*/
void uploadToImgur(){
    CURLcode ret;
    CURL *hnd;
    struct curl_httppost *post1;
    struct curl_httppost *postend;
    struct curl_slist *slist1;
    post1 = NULL;
    postend = NULL;
    curl_formadd(&post1, &postend,
                 CURLFORM_COPYNAME, "image",
                 CURLFORM_FILE, "/tmp/KneckPrint.png",
                 CURLFORM_CONTENTTYPE, "application/octet-stream",
                 CURLFORM_END);
    slist1 = NULL;
    slist1 = curl_slist_append(slist1, "Authorization: Client-ID 006285328c30b34");
    hnd = curl_easy_init();
    curl_easy_setopt(hnd, CURLOPT_URL, "https://api.imgur.com/3/upload");
    curl_easy_setopt(hnd, CURLOPT_HTTPPOST, post1);
    curl_easy_setopt(hnd, CURLOPT_HTTPHEADER, slist1);
    FILE* file = fopen("data", "w");
    curl_easy_setopt(hnd, CURLOPT_WRITEDATA, file);
      ret = curl_easy_perform(hnd);
        curl_easy_cleanup(hnd);
        hnd = NULL;
        curl_formfree(post1);
        post1 = NULL;
        curl_slist_free_all(slist1);
        slist1 = NULL;
    fclose(file);
}

int main(int argc, char *argv[]) {
  
  return 0;

}
